default["elasticsearch"]["clustername"] = "elasticsearch"
default["elasticsearch"]["version"] = "0.19.9"
default["elasticsearch"]["base"] = "/apps/elasticsearch"
default["elasticsearch"]["data"] = "/data/elasticsearch"
default["elasticsearch"]["discoverytype"] = "ec2"
default["elasticsearch"]["securitygroups"] = ["Services","StagingServices"]
