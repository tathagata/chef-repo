
arr_securitygroups=node[:elasticsearch][:securitygroups]
securitygroups=arr_securitygroups.to_s.gsub('"','')
puts "Name of cluster is #{node[:elasticsearch][:clustername]}"
puts "sec is #{securitygroups}"
puts "version is #{node[:elasticsearch][:version]}"
directory "/apps" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

directory "#{node[:elasticsearch][:base]}" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

directory "#{node[:elasticsearch][:data]}" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

bash "setup_elasticsearch" do
	user "root"
	code <<-EOH
	cd /tmp
	wget http://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-#{node[:elasticsearch][:version]}.tar.gz
	tar -zxvf elasticsearch-#{node[:elasticsearch][:version]}.tar.gz
	mv elasticsearch-#{node[:elasticsearch][:version]}/* #{node[:elasticsearch][:base]}
	curl -k -L http://github.com/elasticsearch/elasticsearch-servicewrapper/tarball/master | tar -xz
	mv *servicewrapper*/service #{node[:elasticsearch][:base]}/bin/
	rm -rf *servicewrapper*
	cd #{node[:elasticsearch][:base]}
	#{node[:elasticsearch][:base]}/bin/service/elasticsearch install
	#{node[:elasticsearch][:base]}/bin/plugin -install elasticsearch/elasticsearch-cloud-aws/1.10.0
	rm -rf #{node[:elasticsearch][:base]}/data #{node[:elasticsearch][:base]}/logs
	ln -sf #{node[:elasticsearch][:data]} #{node[:elasticsearch][:base]}/data
	ln -sf /var/log/elasticsearch #{node[:elasticsearch][:base]}/logs
	EOH
	creates "#{node[:elasticsearch][:base]}/bin/elasticsearch"
end

cookbook_file "#{node[:elasticsearch][:base]}/config/logging.yml" do
	source "#{node[:elasticsearch][:base]}/config/logging.yml"
	owner "ec2-user"
	group "ec2-user"
	mode "644"
end

cookbook_file "#{node[:elasticsearch][:base]}/bin/service/elasticsearch.conf" do
	source "#{node[:elasticsearch][:base]}/bin/service/elasticsearch.conf"
	owner "ec2-user"
	group "ec2-user"
	mode "644"
end

template "#{node[:elasticsearch][:base]}/config/elasticsearch.yml" do
	source "#{node[:elasticsearch][:base]}/config/elasticsearch.yml.erb"
	owner "ec2-user"
	group "ec2-user"
	mode "644"
	variables ({
		:cluster_name => node[:elasticsearch][:clustername],
		:access_key => node[:aws][:access_key],
		:secret_key => node[:aws][:secret_key],
		:discovery_type => node[:elasticsearch][:discoverytype],
		:security_groups => securitygroups,
		:region => node[:aws][:region],
		:nodename => node[:fqdn],
		:numshards => node[:elasticsearch][:numshards],
		:numreplicas => node[:elasticsearch][:numreplicas]
	})
end


service "elasticsearch" do
	supports "restart" => true
	action [ :enable, :start ]
end

bash "setup_elastichead" do
	user "root"
	code <<-EOH
	#{node[:elasticsearch][:base]}/bin/plugin -install mobz/elasticsearch-head
	EOH
	creates "#{node[:elasticsearch][:base]}/plugins/head/_site/index.html"
end
