template "/home/hadoop/.bashrc" do
	source "/home/hadoop/bashrc.erb"
	owner "hadoop"
	group "hadoop"
	mode "755"
end

cookbook_file "/home/hadoop/.ssh/id_rsa" do
	source "/home/hadoop/id_rsa"
	owner "hadoop"
	group "hadoop"
	mode "400"
end

cookbook_file "/home/ec2-user/.ssh/id_rsa" do
	source "/home/ec2-user/id_rsa"
	owner "ec2-user"
	group "ec2-user"
	mode "400"
end

directory "/var/log/hadoop" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

directory "/var/log/hbase" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

directory "/var/log/zookeeper" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

directory "/var/run/hadoop" do
    owner "hadoop"
    group "hadoop"
    mode "0755"
end

directory "/apps/releases" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

puts #{node[:hadoop][:fs]}

node[:hadoop][:fs].split(",").each do |fs|

directory "#{fs}" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

end

directory "#{node[:zkfs]}/zoodata" do
	owner "hadoop"
	group "hadoop"
	mode "0755"
end

bash "setup_hadoop" do
	user "root"
	code <<-EOH
	cd /tmp
	wget  http://mysite:8080/tars.tgz
	tar -zxvf tars.tgz && mv tars/* /apps/releases
	ln -sf /var/log/hadoop /apps/releases/hadoop-1.0.4/logs
	ln -sf /apps/releases/hadoop-1.0.4 /apps/hadoop
	ln -sf /var/log/hbase /apps/releases/hbase-0.94.2/logs
	ln -sf /apps/releases/hbase-0.94.2 /apps/hbase
	ln -sf /var/log/zookeeper /apps/releases/zookeeper-3.4.3/logs
	ln -sf /apps/releases/zookeeper-3.4.3 /apps/zookeeper
	chown -R hadoop:hadoop /apps
	rm -rf /tmp/tars*.*
	EOH
	creates "/apps/hadoop"
end	

config_coresite = data_bag_item("#{node[:cluster_name]}","coresite")["#{node.chef_environment}"]

puts config_coresite


template "/apps/hadoop/conf/core-site.xml" do
	source "/apps/hadoop/conf/core-site.xml.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
	    :hadoop_tmp_dir => config_coresite["hadoop.tmp.dir"],
	    :dfs_data_dir => node[:hadoop][:fs],
	    :io_compression_codecs => config_coresite["io.compression.codecs"],
	    :io_compression_codec_lzo_class => config_coresite["io.compression.codec.lzo.class"],
	    :fs_default_name => config_coresite["fs.default.name"],
	    :hadoop_security_authorization => config_coresite["hadoop.security.authorization"],
	    :io_serialization => config_coresite["io.serializations"]
    })
end

config_hdfssite = data_bag_item("#{node[:cluster_name]}","hdfssite")["#{node.chef_environment}"]

template "/apps/hadoop/conf/hdfs-site.xml" do
	source "/apps/hadoop/conf/hdfs-site.xml.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
        :dfs_support_append => config_hdfssite["dfs.support.append"],
        :dfs_data_dir => node[:hadoop][:fs],
        :dfs_datanode_max_xcievers => config_hdfssite["dfs.datanode.max.xcievers"],
        :dfs_hosts_exclude => config_hdfssite["dfs.hosts.exclude"],
        :dfs_replication => config_hdfssite["dfs.replication"],
        :fs_checkpoint_period => config_hdfssite["fs.checkpoint.period"],
        :dfs_http_address => config_hdfssite["dfs.http.address"],
        :dfs_secondary_http_address => config_hdfssite["dfs.secondary.http.address"],
	:fs_s3n_awsAccessKeyId => config_hdfssite["fs.s3n.awsAccessKeyId"],
	:fs_s3n_awsSecretAccessKey => config_hdfssite["fs.s3n.awsSecretAccessKey"]
	})
end

template "/apps/hadoop/conf/hadoop-env.sh" do
	source "/apps/hadoop/conf/hadoop-env.sh.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
		:cluster_name => node[:cluster_name]
	})
end

config_mapredsite = data_bag_item("#{node[:cluster_name]}","mapredsite")["#{node.chef_environment}"]

template "/apps/hadoop/conf/mapred-site.xml" do
	source "/apps/hadoop/conf/mapred-site.xml.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
	    :mapred_jobtracker_taskScheduler => config_mapredsite["mapred.jobtracker.taskScheduler"],
	    :io_sort_record_percent => config_mapredsite["io.sort.record.percent"],
	    :io_sort_spill_percent => config_mapredsite["io.sort.spill.percent"],
	    :io_sort_factor => config_mapredsite["io.sort.factor"],
	    :io_sort_mb => config_mapredsite["io.sort.mb"],
	    :mapred_child_java_opts => config_mapredsite["mapred.child.java.opts"],
	    :mapred_child_ulimit => config_mapredsite["mapred.child.ulimit"],
	    :mapred_job_tracker => config_mapredsite["mapred.job.tracker"],
	    :hbase_regionserver_lease_period => config_mapredsite["hbase.regionserver.lease.period"],
	    :hbase_rpc_timeout => config_mapredsite["hbase.rpc.timeout"],
	    :mapred_job_tracker_handler_count => config_mapredsite["mapred.job.tracker.handler.count"],
	    :mapred_map_tasks_speculative_execution => config_mapredsite["mapred.map.tasks.speculative.execution"],
	    :mapred_reduce_parallel_copies => config_mapredsite["mapred.reduce.parallel.copies"],
	    :mapred_reduce_tasks => config_mapredsite["mapred.reduce.tasks"],
	    :mapred_reduce_tasks_speculative_execution => config_mapredsite["mapred.reduce.tasks.speculative.execution"],
	    :mapred_tasktracker_map_tasks_maximum => config_mapredsite["mapred.tasktracker.map.tasks.maximum"],
	    :mapred_tasktracker_reduce_tasks_maximum => config_mapredsite["mapred.tasktracker.reduce.tasks.maximum"],
	    :tasktracker_http_threads => config_mapredsite["tasktracker.http.threads"],
	    :mapred_output_compression_type => config_mapredsite["mapred.output.compression.type"],
	    :mapred_compress_map_output => config_mapredsite["mapred.compress.map.output"],
	    :mapred_output_compression_codec => config_mapredsite["mapred.output.compression.codec"],
	    :mapred_map_output_compression_codec => config_mapredsite["mapred.map.output.compression.codec"],
	    :hadoop_rpc_socket_factory_class_default => config_mapredsite["hadoop.rpc.socket.factory.class.default"],
	    :mapred_job_reuse_jvm_num_tasks => config_mapredsite["mapred.job.reuse.jvm.num.tasks"],
	    :mapred_capacity_scheduler_queue_default_capacity => config_mapredsite["mapred.capacity-scheduler.queue.default.capacity"],
	    :mapred_capacity_scheduler_queue_default_supports_priority => config_mapredsite["mapred.capacity-scheduler.queue.default.supports-priority"],
	    :mapred_capacity_scheduler_queue_default_minimum_user_limit_percent => config_mapredsite["mapred.capacity-scheduler.queue.default.minimum-user-limit-percent"],
	    :mapred_capacity_scheduler_queue_default_maximum_initialized_jobs_per_user => config_mapredsite["mapred.capacity-scheduler.queue.default.maximum-initialized-jobs-per-user"],
	    :mapred_capacity_scheduler_default_supports_priority => config_mapredsite["mapred.capacity-scheduler.default-supports-priority"],
	    :mapred_capacity_scheduler_default_minimum_user_limit_percent => config_mapredsite["mapred.capacity-scheduler.default-minimum-user-limit-percent"],
	    :mapred_capacity_scheduler_default_maximum_initialized_jobs_per_user => config_mapredsite["mapred.capacity-scheduler.default-maximum-initialized-jobs-per-user"],
	    :mapred_capacity_scheduler_init_poll_interval => config_mapredsite["mapred.capacity-scheduler.init-poll-interval"],
	    :mapred_capacity_scheduler_init_worker_threads => config_mapredsite["mapred.capacity-scheduler.init-worker-threads"],
	    :mapred_reduce_slowstart_completed_maps => config_mapredsite["mapred.reduce.slowstart.completed.maps"],
	    :mapred_task_timeout => config_mapredsite["mapred.task.timeout"]
    })
end

[ "lzo","lzo-devel","lzop" ].each do |pkg|
	package pkg
end

cookbook_file "/etc/security/limits.conf" do
	source "/etc/security/limits.conf"
	owner "root"
	group "root"
	mode "0644"
end

cookbook_file "/etc/pam.d/su" do
	source "/etc/pam.d/su"
	owner "root"
	group "root"
	mode "0644"
end

cookbook_file "/etc/pam.d/login" do
	source "/etc/pam.d/login"
	owner "root"
	group "root"
	mode "0644"
end

config_base = data_bag_item("#{node[:cluster_name]}","base")["#{node.chef_environment}"]

template "/apps/hadoop/conf/hadoop-metrics2.properties" do
    source "/apps/hadoop/conf/hadoop-metrics2.properties.erb"
    owner "hadoop"
    group "hadoop"
    mode "0644"
    variables ({
        :ganglia_server => config_base["ganglia-server"],
        :ganglia_port => config_base["ganglia-port"]
    })
end

template "/etc/ganglia/gmond.conf" do
    source "/etc/ganglia/gmond.conf.erb"
    owner "root"
    group "root"
    mode "0644"
    variables ({
        :ganglia_server => config_base["ganglia-server"],
        :ganglia_port => config_base["ganglia-port"],
        :ganglia_location => config_base["ganglia-location"]
    })
end

servers = Array.new
search(:node,"chef_environment:#{node.chef_environment} AND role:datanode AND cluster_name:#{node.cluster_name}").each do |server|
	servers << server[:fqdn]
end

template "/apps/hadoop/conf/slaves" do
    source "/apps/hadoop/conf/slaves.erb"
    owner "hadoop"
    group "hadoop"
    mode "0644"
    variables ({
        :servers => servers
    })
end

servers = Array.new
search(:node,"chef_environment:#{node.chef_environment} AND role:tasktracker AND cluster_name:#{node.cluster_name}").each do |server|
	servers << server[:fqdn]
end

template "/apps/hadoop/conf/mrnodes" do
    source "/apps/hadoop/conf/mrnodes.erb"
    owner "hadoop"
    group "hadoop"
    mode "0644"
    variables ({
        :servers => servers
    })
end

if node.role?("tasktracker")
	bash "start_tasktracker" do
		user "hadoop"
		code <<-EOH 
			HOME=/home/hadoop
			USER=hadoop
			source ~/.bashrc
			hadoop-daemon.sh start tasktracker
		EOH
		creates "/var/run/hadoop/hadoop-hadoop-tasktracker.pid"
		
	end
end
