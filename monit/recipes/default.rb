
package "monit" do
	action :install
end

cookbook_file "/etc/monit.d/gnip.monitrc" do
	source "#{node[:servicename]}/etc/monit.d/gnip.monitrc"
	owner "root"
	group "root"
	mode "0644"
end

service "monit" do
	supports "restart" => true
   	action [ :enable, :start ]
	subscribes :restart, resources("cookbook_file[/etc/monit.d/gnip.monitrc]"), :immediately
end
