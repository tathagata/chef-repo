servers = Array.new
search(:node,"chef_environment:#{node.chef_environment} AND role:datanode AND cluster_name:#{node.cluster_name}").each do |server|
	servers << server[:fqdn]
end

config_hbase = data_bag_item("#{node[:cluster_name]}","hbase")["#{node.chef_environment}"]

template "/apps/hbase/conf/hbase-site.xml" do
	source "/apps/hbase/conf/hbase-site.xml.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
	    :hbase_rootdir => config_hbase["hbase.rootdir"],
	    :hbase_cluster_distributed => config_hbase["hbase.cluster.distributed"],
	    :hbase_zookeeper_quorum => config_hbase["hbase.zookeeper.quorum"],
	    :hbase_hregion_max_filesize => config_hbase["hbase.hregion.max.filesize"],
	    :hbase_regionserver_handler_count => config_hbase["hbase.regionserver.handler.count"],
	    :hbase_regionserver_global_memstore_lowerLimit => config_hbase["hbase.regionserver.global.memstore.lowerLimit"],
	    :hbase_regionserver_global_memstore_upperLimit => config_hbase["hbase.regionserver.global.memstore.upperLimit"]       
	}) 	
end

link "/apps/hadoop/conf/hbase-site.xml" do
    to "/apps/hbase/conf/hbase-site.xml"
    owner "hadoop"
    group "hadoop"
end

template "/apps/hbase/conf/hbase-env.sh" do
	source "/apps/hbase/conf/hbase-env.sh.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
		:hbase_heapsize => config_hbase["heapsize"]
	})
end

template "/apps/hbase/conf/regionservers" do
	source "/apps/hbase/conf/regionservers.erb"
	owner "hadoop"
	group "hadoop"
	mode "0644"
	variables ({
		:servers => servers
	})
end

config_base = data_bag_item("#{node[:cluster_name]}","base")["#{node.chef_environment}"]

template "/apps/hbase/conf/hadoop-metrics.properties" do
    source "/apps/hbase/conf/hadoop-metrics.properties.erb"
    owner "hadoop"
    group "hadoop"
    mode "0644"
    variables ({
        :ganglia_server => config_base["ganglia_server"],
        :ganglia_port => config_base["ganglia_port"]
    })
end

cookbook_file "/apps/hbase/lib/hbaseserverlib-0.1-SNAPSHOT.jar" do
	source "/apps/hbase/lib/hbaseserverlib-0.1-SNAPSHOT.jar"
	owner "hadoop"
	group "hadoop"
	mode "0644"
end
