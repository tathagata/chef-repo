default["solr"]["version"] = "4.0.0"
default["solr"]["numShards"] = "2"
default["solr"]["fs"] = "/data/solr"
default["solr"]["base"] = "/apps/solr"

