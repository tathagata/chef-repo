
config_solr = data_bag_item("solr","collections")
collection_names=["latest","historical"]
shard_names=["shard1","shard2"]
instance_base=["/apps/solr/example","/apps/solr/example2"]

directory "/apps/releases" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

bash "setup_solr" do
	user "root"
	code <<-EOH
	cd /tmp
	ln -sf /apps/releases/apache-solr-4.1.0 #{node[:solr][:base]}
	chown -R ec2-user:ec2-user /apps/releases/apache-solr-4.1.0
	chown -R ec2-user:ec2-user /apps/solr
	EOH
	creates #{node[:solr][:base]
end

node[:solr][:fs].split(",").each do |fs|
directory "#{fs}" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
	recursive true
end
end

collection_names.each do |collection|
        shard_names.each do |shard|
		app_dir = config_solr["#{collection}"]["#{shard}"]["app_dir"]
		data_dir = config_solr["#{collection}"]["#{shard}"]["data_dir"]
                directory "#{app_dir}" do
                        owner "ec2-user"
                        group "ec2-user"
                        mode "0755"
                        recursive true
                end
                directory "#{data_dir}" do
                        owner "ec2-user"
                        group "ec2-user"
                        mode "0755"
                        recursive true
                end
		link "#{app_dir}/data" do
			to "#{data_dir}"
			owner "ec2-user"
			group "ec2-user"
		end
		directory "#{app_dir}/conf" do
			owner "ec2-user"
			group "ec2-user"
			mode "0755"
			recursive true
		end
        end     
end

instance_base.each do |base_dir|
	cookbook_file "#{base_dir}/solr/solr.xml" do
		source "solr.xml"
		owner "ec2-user"
		group "ec2-user"
		mode "0644"
	end
end

directory "/var/log/solr" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

directory "/var/run/solr" do
	owner "ec2-user"
	group "ec2-user"
	mode "0755"
end

num_projects = case node.chef_environment
	when "staging-mr" 
		2
	when "staging"
		2
	when "production-mr"
		2
	when "production"
		2
end

template "/etc/init.d/solr" do
	source "/etc/init.d/solr.erb"
	user "root"
	group "root"
	mode "0755"
	variables ({
		:num_projects => num_projects,
		:zookeeper_quorum => zookeeper_quorum
	})
end

config_jetty = data_bag_item("solr","jetty")["#{node.chef_environment}"]

template "/apps/solr/example/etc/jetty.xml" do
	source "/apps/solr/example/etc/jetty.xml.erb"
	owner "ec2-user"
	group "ec2-user"
	mode "0644"
	variables ({
	    :maxFormContentSize => config_jetty["org.eclipse.jetty.server.Request.maxFormContentSize"]       
	}) 	
end

template "/apps/solr/example2/etc/jetty.xml" do
	source "/apps/solr/example2/etc/jetty.xml.erb"
	owner "ec2-user"
	group "ec2-user"
	mode "0644"
	variables ({
	    :maxFormContentSize => config_jetty["org.eclipse.jetty.server.Request.maxFormContentSize"]       
	}) 	
end

service "solr" do
	supports "restart" => true
	action [ :start ]
end

